package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

type SessionRequest struct {
	ViewDurationSeconds   int    `json:"viewDurationSeconds"`
	TimestampSessionClose string `json:"timestampSessionClose"`
}

func main() {
	httpClient := createClient()
	var wg sync.WaitGroup

	for i := 0; i < 100; i++ {
		wg.Add(1)
		go startLoadTest(httpClient, &wg)
	}

	wg.Wait()
}

func startLoadTest(client *http.Client, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 0; i < 10000; i++ {
		timeOffset := rand.Intn(3500)
		sessionRequest := SessionRequest{
			ViewDurationSeconds:   rand.Intn(10000) + 1,
			TimestampSessionClose: time.Now().Add(time.Second * -1 * time.Duration(timeOffset)).Format(time.RFC3339),
		}
		jsonValue, _ := json.Marshal(sessionRequest)
		start := time.Now()
		resp, err := client.Post("http://localhost:8080/sessions", "application/json", bytes.NewBuffer(jsonValue))
		if err != nil {
			panic(err)
		}
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()
		log.Printf("Finished request within %v", time.Since(start).Milliseconds())
	}
}

func createClient() *http.Client {
	defaultRoundTripper := http.DefaultTransport
	defaultTransportPointer, ok := defaultRoundTripper.(*http.Transport)
	if !ok {
		panic("defaultRoundTripper not an *http.Transport")
	}
	defaultTransport := *defaultTransportPointer
	defaultTransport.MaxIdleConns = 100
	defaultTransport.MaxIdleConnsPerHost = 100

	return &http.Client{Transport: &defaultTransport}
}
