package io.gitlab.michaelbgd.statistics

interface StatisticsService {
    fun record(session: Session)
    fun stats(): Statistics
    fun clear()
}

fun interface TimeProvider {
    fun getTime(): Long
}

object SessionExpired : Exception("Session older than one hour")
object SessionInFuture : Exception("Session timestamp is in the future")