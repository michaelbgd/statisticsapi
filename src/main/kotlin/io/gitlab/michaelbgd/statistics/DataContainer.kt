package io.gitlab.michaelbgd.statistics

import java.math.BigDecimal
import java.util.concurrent.ConcurrentHashMap

class DataContainer {
    private val data = ConcurrentHashMap<Long, StatsPerSecond>()

    fun store(now: Long, timestamp: Long, duration: Long) {
        data.computeIfAbsent(timestamp) {
            clearOldRecords(now)
            StatsPerSecond()
        }.apply {
            measureTime {
                synchronized(this) {
                    val sessionDuration = duration.toBigDecimal()
                    averageSeconds =
                        (averageSeconds * amountRecords + sessionDuration.precision4()) / amountRecords.inc()
                    amountRecords = amountRecords.inc()
                    if (sessionDuration > maxDuration) {
                        maxDuration = sessionDuration
                    }
                    if (sessionDuration < minDuration || minDuration == BigDecimal.valueOf(0)) {
                        minDuration = sessionDuration
                    }
                }

            }.let { (_, time) ->
                if (time > 5) {
                    LOGGER.warn("Recording time $time ms")
                }
            }
        }
    }

    fun collect(start: Long) = data.filterKeys { it >= start }.values.measureTime { list ->
        if (list.isEmpty()) {
            null
        } else {
            val numberSessions = list.sumOf { it.amountRecords }
            val averageSessionDuration =
                list.sumOf { it.averageSeconds * it.amountRecords }.precision4() / numberSessions
            val minSessionDuration = list.minOf { it.minDuration }
            val maxSessionDuration = list.maxOf { it.maxDuration }
            Statistics(
                averageSessionDuration = averageSessionDuration.precision2(),
                minSessionDuration = minSessionDuration,
                maxSessionDuration = maxSessionDuration,
                numberSessions = numberSessions
            )
        }
    }.let { (statistics, time) ->
        if (time > 5) {
            LOGGER.warn("Collection time $time ms")
        }
        statistics
    }

    fun clear() = data.clear()

    private fun clearOldRecords(now: Long) {
        data.keys().asSequence().filter { it < now - 3600 }.forEach { data.remove(it) }
    }

    private data class StatsPerSecond(
        var averageSeconds: BigDecimal = BigDecimal.valueOf(0),
        var amountRecords: BigDecimal = BigDecimal.valueOf(0),
        var maxDuration: BigDecimal = BigDecimal.valueOf(0),
        var minDuration: BigDecimal = BigDecimal.valueOf(0),
    )

    private companion object : LoggerAware()
}
