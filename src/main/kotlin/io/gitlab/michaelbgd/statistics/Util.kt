package io.gitlab.michaelbgd.statistics

import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

internal fun String.toSeconds(): Long =
    LocalDateTime.parse(this, DateTimeFormatter.ISO_DATE_TIME).toEpochSecond(ZoneOffset.UTC)

internal fun BigDecimal.precision2(): BigDecimal = setScale(2, BigDecimal.ROUND_HALF_UP)
internal fun BigDecimal.precision4(): BigDecimal = setScale(4, BigDecimal.ROUND_HALF_UP)

inline fun <reified T, A> A.measureTime(func: (A) -> T): Pair<T, Long> {
    val start = now()
    return func(this) to now() - start
}

fun now() = System.currentTimeMillis()

abstract class LoggerAware {
    val LOGGER = LoggerFactory.getLogger(this.javaClass.enclosingClass)
}