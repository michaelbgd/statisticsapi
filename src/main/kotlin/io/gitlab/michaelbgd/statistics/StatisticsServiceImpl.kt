package io.gitlab.michaelbgd.statistics

import java.math.BigDecimal

class StatisticsServiceImpl(
    private val dataContainer: DataContainer,
    private val timeProvider: TimeProvider
) : StatisticsService {
    override fun record(session: Session) {
        val now = timeProvider.getTime()
        val timestamp = session.timestampSessionClose.toSeconds()

        when {
            now - timestamp > 3600 -> throw SessionExpired
            timestamp > now -> throw SessionInFuture
        }

        dataContainer.store(
            now,
            timestamp,
            session.viewDurationSeconds
        )
    }

    override fun stats(): Statistics = dataContainer.collect(timeProvider.getTime() - 3600) ?: Statistics(
        BigDecimal.valueOf(0).precision2(),
        BigDecimal.valueOf(0),
        BigDecimal.valueOf(0),
        BigDecimal.valueOf(0)
    ).apply {
        LOGGER.info("No records available, returning empty object")
    }

    override fun clear() = dataContainer.clear().apply {
        LOGGER.info("Clearing records")
    }

    private companion object : LoggerAware()
}
