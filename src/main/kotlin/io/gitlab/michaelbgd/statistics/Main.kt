package io.gitlab.michaelbgd.statistics

import com.fasterxml.jackson.core.JsonProcessingException
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeParseException

fun main() {
    applicationEngine {
        LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)
    }.start(wait = true)
}

fun applicationEngine(timeProvider: TimeProvider) = embeddedServer(Netty, port = 8080) {
    val statisticsService = StatisticsServiceImpl(DataContainer(), timeProvider)
    configureRouting(statisticsService)
}

fun Application.configureRouting(statisticsService: StatisticsService) {
    val logger = object : LoggerAware() {}.LOGGER
    install(ContentNegotiation) {
        jackson()
    }
    install(StatusPages) {
        exception<SessionExpired> { _ ->
            call.respond(HttpStatusCode.NoContent)
        }
        exception<JsonProcessingException> { _ ->
            call.respond(HttpStatusCode.BadRequest)
        }
        exception<DateTimeParseException> { _ ->
            call.respond(HttpStatusCode.BadRequest)
        }
        exception<SessionInFuture> { _ ->
            call.respond(HttpStatusCode.UnprocessableEntity)
        }
        exception<Throwable> { e ->
            logger.warn("Request caused unexpected exception", e)
            call.respond(HttpStatusCode.InternalServerError)
        }
    }
    routing {
        get("/statistics") {
            call.respond(statisticsService.stats())
        }
        post("/sessions") {
            val session = call.receive<Session>()
            statisticsService.record(session)
            call.respond(HttpStatusCode.Created)
        }
        delete("/sessions") {
            statisticsService.clear()
            call.respond(HttpStatusCode.NoContent)
        }
    }
}

data class Session(
    val timestampSessionClose: String,
    val viewDurationSeconds: Long
)

data class Statistics(
    val averageSessionDuration: BigDecimal,
    val minSessionDuration: BigDecimal,
    val maxSessionDuration: BigDecimal,
    val numberSessions: BigDecimal,
)
