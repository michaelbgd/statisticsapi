package io.gitlab.michaelbgd.statistics

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import kotlin.random.Random
import kotlin.random.nextInt
import kotlin.test.Test
import kotlin.test.assertEquals


class E2eTests {
    private val server = applicationEngine {
        LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)
    }

    @Before
    fun startServer() {
        server.start(wait = false)
    }

    @After
    fun stopServer() {
        server.stop(100, 100)
    }

    @Test
    fun test() = runBlocking {
        val client = createClient()

        var min: Long? = null
        var max: Long? = null
        val sessions = (1..10000).map {
            val time = LocalDateTime.now().minusSeconds(Random.nextInt(10..60).toLong())
            val duration = Random.nextInt(10..20000).toLong()

            duration to async {
                client.post<HttpResponse>("http://127.0.0.1:8080/sessions") {
                    contentType(ContentType.Application.Json)
                    body = Session(
                        timestampSessionClose = time.format(DateTimeFormatter.ISO_DATE_TIME),
                        viewDurationSeconds = duration
                    )
                }
            }
        }
        val okSessions = sessions.mapNotNull { (duration, response) ->
            val httpResponse = response.await()
            if (httpResponse.status == HttpStatusCode.Created) {
                if (min == null || duration < min!!) min = duration
                if (max == null || duration > max!!) max = duration
                duration
            } else {
                null
            }
        }
        val avg = okSessions.average()
        val response: HttpResponse = client.get("http://127.0.0.1:8080/statistics")
        val numSessions = okSessions.size
        val expected = statsJson(avg, min, max, numSessions)
        assertEquals(expected, response.readText())
    }

    private fun createClient() = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }
    }
}