package io.gitlab.michaelbgd.statistics

import java.math.BigDecimal

fun statsJson(
    avg: Double = 0.0,
    min: Long? = 0,
    max: Long? = 0,
    numSessions: Int = 0
) =
    """{"averageSessionDuration":${
        BigDecimal.valueOf(avg).setScale(2, BigDecimal.ROUND_HALF_UP)
    },"minSessionDuration":$min,"maxSessionDuration":$max,"numberSessions":$numSessions}"""

fun sessionStoreRequestJson(timestamp: String, duration: Int) =
    """{"timestampSessionClose": "$timestamp", "viewDurationSeconds": $duration}"""