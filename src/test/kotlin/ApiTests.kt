package io.gitlab.michaelbgd.statistics

import io.ktor.http.*
import io.ktor.server.testing.*
import org.junit.After
import java.time.LocalDateTime
import java.time.ZoneOffset
import kotlin.test.Test
import kotlin.test.assertEquals

class ApiTests {
    private val timeProvider = { LocalDateTime.parse("2021-08-19T14:00:00").toEpochSecond(ZoneOffset.UTC) }
    private val dataContainer = DataContainer()
    private val statisticsService: StatisticsService = StatisticsServiceImpl(dataContainer, timeProvider)

    @After
    fun cleanup() {
        dataContainer.clear()
    }

    @Test
    fun testEmptyStats() {
        withTestApplication({ configureRouting(statisticsService) }) {
            handleRequest(HttpMethod.Get, "/statistics").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(
                    statsJson(),
                    response.content
                )
            }
        }
    }

    @Test
    fun testMalformedJson() = withTestApplication({ configureRouting(statisticsService) }) {
        with(handleRequest(HttpMethod.Post, "/sessions") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody("""{"timestampSessionClose": "2021-08-19T13:58:36Z", "viewDurationSeconds": 250""")
        }) {
            assertEquals(HttpStatusCode.BadRequest, response.status())
        }
    }

    @Test
    fun testTimeParseError() = withTestApplication({ configureRouting(statisticsService) }) {
        with(handleRequest(HttpMethod.Post, "/sessions") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(sessionStoreRequestJson("""2021-08-19T12:5836Z""", 250))
        }) {
            assertEquals(HttpStatusCode.BadRequest, response.status())
        }
    }

    @Test
    fun testSessionExpired() = withTestApplication({ configureRouting(statisticsService) }) {
        with(handleRequest(HttpMethod.Post, "/sessions") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(sessionStoreRequestJson("""2021-08-19T12:58:36Z""", 250))
        }) {
            assertEquals(HttpStatusCode.NoContent, response.status())
        }
    }

    @Test
    fun testSessionInFuture() = withTestApplication({ configureRouting(statisticsService) }) {
        with(handleRequest(HttpMethod.Post, "/sessions") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(sessionStoreRequestJson("""2021-08-19T14:58:36Z""", 250))
        }) {
            assertEquals(HttpStatusCode.UnprocessableEntity, response.status())
        }
    }

    @Test
    fun testDelete() = withTestApplication({ configureRouting(statisticsService) }) {
        with(handleRequest(HttpMethod.Post, "/sessions") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(sessionStoreRequestJson("""2021-08-19T13:58:36Z""", 250))
        }) {
            assertEquals(HttpStatusCode.Created, response.status())
        }
        with(handleRequest(HttpMethod.Get, "/statistics")) {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals(
                statsJson(250.0, 250, 250, 1),
                response.content
            )
        }
        with(handleRequest(HttpMethod.Delete, "/sessions")) {
            assertEquals(HttpStatusCode.NoContent, response.status())
        }
        with(handleRequest(HttpMethod.Get, "/statistics")) {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals(
                statsJson(),
                response.content
            )
        }
    }

    @Test
    fun testRequests() = withTestApplication({ configureRouting(statisticsService) }) {
        with(handleRequest(HttpMethod.Post, "/sessions") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            val duration = 250
            val timestamp = """2021-08-19T13:58:36Z"""
            setBody(sessionStoreRequestJson(timestamp, duration))
        }) {
            assertEquals(HttpStatusCode.Created, response.status())
        }
        with(handleRequest(HttpMethod.Post, "/sessions") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            val duration = 800
            val timestamp = """2021-08-19T13:58:37Z"""
            setBody(sessionStoreRequestJson(timestamp, duration))
        }) {
            assertEquals(HttpStatusCode.Created, response.status())
        }
        with(handleRequest(HttpMethod.Post, "/sessions") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            val duration = 200
            val timestamp = """2021-08-19T13:58:37Z"""
            setBody(sessionStoreRequestJson(timestamp, duration))
        }) {
            assertEquals(HttpStatusCode.Created, response.status())
        }
        with(handleRequest(HttpMethod.Get, "/statistics")) {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals(
                statsJson(416.67, 200, 800, 3),
                response.content
            )
        }
    }
}