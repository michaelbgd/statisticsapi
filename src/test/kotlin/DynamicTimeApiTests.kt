package io.gitlab.michaelbgd.statistics

import io.ktor.http.*
import io.ktor.server.testing.*
import org.junit.After
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import kotlin.test.Test
import kotlin.test.assertEquals

class DynamicTimeApiTests {
    private val timeProvider = object : TimeProvider {
        var dateTime = LocalDateTime.parse("2021-08-19T14:00:00")
        override fun getTime(): Long = dateTime.toEpochSecond(ZoneOffset.UTC)
        fun increment() {
            dateTime = dateTime.plusSeconds(1)
        }
    }
    private val dataContainer = DataContainer()
    private val statisticsService: StatisticsService = StatisticsServiceImpl(dataContainer, timeProvider)

    @After
    fun cleanup() {
        dataContainer.clear()
    }

    @Test
    fun testCornerCase() = withTestApplication({ configureRouting(statisticsService) }) {
        with(handleRequest(HttpMethod.Post, "/sessions") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            val duration = 250
            val timestamp = timeProvider.dateTime.minusHours(1).format(DateTimeFormatter.ISO_DATE_TIME)
            setBody(sessionStoreRequestJson(timestamp, duration))
        }) {
            assertEquals(HttpStatusCode.Created, response.status())
        }
        with(handleRequest(HttpMethod.Get, "/statistics")) {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals(
                statsJson(250.00, 250, 250, 1),
                response.content
            )
        }
        timeProvider.increment()
        with(handleRequest(HttpMethod.Get, "/statistics")) {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals(statsJson(), response.content)
        }
    }
}